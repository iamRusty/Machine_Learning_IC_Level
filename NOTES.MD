# NOTES: Notes after reading the papers

1. [Hardware Implmentation of Artificial Neural Networks](https://users.ece.cmu.edu/~pgrover/teaching/files/NeuromorphicComputing.pdf) by Mats Forssell, 2014
- von Neumann bottleneck - limited data transfer rate (throughput) because of memory access time. A single bus can only access one of the two classes of memory at a time.
- Challenges in scaling of the networks since the number of synapses grow QUADRATICALLY with the number of neurons, making wiring difficult
- 3 Training Strategies
- Off-chip learning - learning stage is done in a simulated network in a network
- Chip-in-the-loop - software performs learning algorithm while hardware network performs computations
- On-chip learning - Uses only hardware chip to perform learning. Complicated and less flexible
- 